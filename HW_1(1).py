def parse_cookie(query: str) -> dict:
    params = query.split(';')
    my_dict = {}
    for i in params:
        if "=" in i:
            x = i.split('=', 1)
            my_dict.update({x[0]: x[1]})
    return my_dict


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}

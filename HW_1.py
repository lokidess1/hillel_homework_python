def parse(query: str) -> dict:
    if '?' in query:
        params = query.split('?')[1].split('&')
        my_dict = {}
        for i in params:
            if len(i) > 0:
                x = i.split('=')
                my_dict.update({x[0]: x[1]})
        return my_dict
    else:
        return {}


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}

